#!/usr/bin/python
# -*- coding: utf-8 -*-

import Tkinter
import tkFileDialog
import tkMessageBox
import hashlib


class simpleapp_tk(Tkinter.Tk):
	def __init__(self,parent):
		Tkinter.Tk.__init__(self,parent)
		self.parent = parent
		self.bind('<Control-c>', self.copy)
		self.bind('<Control-x>', self.cut)
		self.bind('<Control-v>', self.paste)
		self.bind('<Control-a>', self.selectall)
		self.initialize()

	def initialize(self):
		self.grid()
		
		
		#######
		#######
		####### TEXT VAR ENTRY
		self.text1 = Tkinter.StringVar()
		self.textbox = Tkinter.Text(self)
		self.textbox.grid(column=0, row=0, sticky="EW")
		#######
		#######
		
		
		
		button = Tkinter.Button(self,text=u"Hash Away!", command=self.OnButtonClick)
		button.grid(column=2,row=1)
		
		self.optionslist = ('ROT13','SHA-1','SHA-224','SHA-256','SHA-384','SHA-512','MD5')
		self.var = Tkinter.StringVar(self)
		self.var.set(self.optionslist[0])
		self.optmenu = apply(Tkinter.OptionMenu,(self, self.var)+self.optionslist)
		self.optmenu.grid(column=2,row=0)
		
		

		self.grid_columnconfigure(0,weight=1)
		self.grid_rowconfigure(0,weight=1)
		self.resizable(True,False)
		self.update()
		self.geometry(self.geometry())       
		self.textbox.focus_set()
		#self.entry.selection_range(0, Tkinter.END)
		
		
		
		############
		############
		########### Menu
		###########
		menubar = Tkinter.Menu(self)
		filemenu = Tkinter.Menu(menubar, tearoff=0)
		filemenu.add_command(label="New", command=self.Newbox)
		filemenu.add_command(label="Open", command=self.Openfile)
		filemenu.add_command(label="Save", command=self.SaveFile)
		filemenu.add_command(label="Save as...", command=self.SaveFile)
		filemenu.add_command(label="Close", command=self.Newbox)

		filemenu.add_separator()

		filemenu.add_command(label="Exit", command=self.quit)
		menubar.add_cascade(label="File", menu=filemenu)
		editmenu = Tkinter.Menu(menubar, tearoff=0)

		editmenu.add_command(label="Cut", command=self.cut)
		editmenu.add_command(label="Copy", command=self.copy)
		editmenu.add_command(label="Paste", command=self.paste)
		editmenu.add_command(label="Delete", command=self.delete)
		editmenu.add_command(label="Select All", command=self.selectall)

		menubar.add_cascade(label="Edit", menu=editmenu)
		###
		
		###
		aboutmenu = Tkinter.Menu(menubar, tearoff=0)
		aboutmenu.add_command(label="About...", command=self.about)
		menubar.add_cascade(label="About", menu=aboutmenu)

		self.config(menu=menubar)
        ##########END MENU
		###########
		##########
		##########
		
		
	def OnButtonClick(self):
		if self.var.get() == 'ROT13':
			self.rough1 = self.textbox.get("0.0",Tkinter.END)
			self.textbox.delete("0.0", Tkinter.END)
			self.textbox.insert('0.0',self.Rot13a(self.rough1).strip())
		if self.var.get() == 'SHA-256':
			self.rough1 = self.textbox.get("0.0",Tkinter.END)
			self.textbox.delete("0.0", Tkinter.END)
			self.textbox.insert('0.0',self.SHA256(self.rough1).strip())
		if self.var.get() == 'SHA-512':
			self.rough1 = self.textbox.get("0.0",Tkinter.END)
			self.textbox.delete("0.0", Tkinter.END)
			self.textbox.insert('0.0',self.SHA512(self.rough1).strip())
		if self.var.get() == 'SHA-1':
			self.rough1 = self.textbox.get("0.0",Tkinter.END)
			self.textbox.delete("0.0", Tkinter.END)
			self.textbox.insert('0.0',self.SHA1(self.rough1).strip())
		if self.var.get() == 'SHA-224':
			self.rough1 = self.textbox.get("0.0",Tkinter.END)
			self.textbox.delete("0.0", Tkinter.END)
			self.textbox.insert('0.0',self.SHA224(self.rough1).strip())	
		if self.var.get() == 'SHA-384':
			self.rough1 = self.textbox.get("0.0",Tkinter.END)
			self.textbox.delete("0.0", Tkinter.END)
			self.textbox.insert('0.0',self.SHA384(self.rough1).strip())	
		if self.var.get() == 'MD5':
			self.rough1 = self.textbox.get("0.0",Tkinter.END)
			self.textbox.delete("0.0", Tkinter.END)
			self.textbox.insert('0.0',self.MD5(self.rough1).strip())
		
        
        
	def Rot13a(self, text):
		abc = list("abcdefghijklmnopqrstuvwxyz")
		ABC = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
		lst = list(text)
		newlst = []
		for i in lst:
			if i in abc:
				ind = abc.index(i)
				ind = (ind + 13)%26
				newlst.append(abc[ind])
			elif i in ABC:
				ind = ABC.index(i)
				ind = (ind + 13)%26
				newlst.append(ABC[ind])
			else:
				newlst.append(i)
		txt = ""
		text = txt.join(newlst)
		return text
	
	def SHA256(self, string):
		return hashlib.sha256(string).hexdigest()
	
	def SHA512(self, string):
		return hashlib.sha512(string).hexdigest()
	
	def SHA1(self, string):
		return hashlib.sha1(string).hexdigest()
	
	def MD5(self, string):
		return hashlib.md5(string).hexdigest()
		
	def SHA224(self, string):
		return hashlib.sha224(string).hexdigest()
		
	def SHA384(self, string):
		return hashlib.sha384(string).hexdigest()

	def Newbox(self):
		self.textbox.delete("0.0", Tkinter.END)
		
	def Openfile(self):
		mask = \
		[("Text and Python files","*.txt *.py *.pyw"),
		("HTML files","*.htm"),
		("All files","*.*")]
		fin = tkFileDialog.askopenfile(initialdir='/', filetypes=mask, mode='r')
		text = fin.read()
		if text != None:
			self.textbox.delete(0.0, Tkinter.END)
			self.textbox.insert(Tkinter.END,text)
	
	def SaveFile(self):
		"""get a filename and save the text in the editor widget"""
		# default extension is optional, here will add .txt if missing
		fout = tkFileDialog.asksaveasfile(mode='w', defaultextension=".txt")
		text2save = str(self.textbox.get(0.0,Tkinter.END))
		fout.write(text2save)
		fout.close()
		
	def copy(self, event):
		self.clipboard_clear()
		text = self.textbox.get("sel.first", "sel.last")
		self.clipboard_append(text)
    
	def cut(self, event):
		self.copy()
		self.textbox.delete("sel.first", "sel.last")

	def paste(self):
		text = self.selection_get(selection='CLIPBOARD')
		self.textbox.insert('insert', text)		
	
	def delete(self):
		self.textbox.delete("sel.first", "sel.last")	
		
	def selectall(self, event):
		self.textbox.tag_add('sel', '1.0', 'end')	
		
	def about(self):
		tkMessageBox.showinfo("About", """HashTool

Copyright 2014 l3li3l@torguard.tg

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. 
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.""")
	
	def donothing(self):
		filewin = Tkinter.Toplevel(self)
		button = Tkinter.Button(filewin, text="Do nothing button")
		button.pack()

if __name__ == "__main__":
	app = simpleapp_tk(None)
	app.title('Hash Tool')
	app.mainloop()
